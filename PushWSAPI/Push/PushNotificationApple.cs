using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PushSharp;
using PushSharp.Apple;
using System.IO;
using System.Diagnostics;
using PushWSAPI.Models;

namespace PushNotification
{
    public class PushNotificationApple
    {
        private static PushBroker _pushBroker;
        private TextWriter txt;
        public PushNotificationApple()
        {
          //  txt = new StreamWriter(@"C:\Pushlog.txt");
            Debug.Write("entre");

            if (_pushBroker == null)
            {
                //Create our push services broker
                _pushBroker = new PushBroker();

                //Wire up the events for all the services that the broker registers
                _pushBroker.OnNotificationSent += NotificationSent;
                _pushBroker.OnChannelException += ChannelException;
                _pushBroker.OnServiceException += ServiceException;
                _pushBroker.OnNotificationFailed += NotificationFailed;
                _pushBroker.OnDeviceSubscriptionExpired += DeviceSubscriptionExpired;
                _pushBroker.OnDeviceSubscriptionChanged += DeviceSubscriptionChanged;
                _pushBroker.OnChannelCreated += ChannelCreated;
                _pushBroker.OnChannelDestroyed += ChannelDestroyed;
                var appleCert = File.ReadAllBytes(System.Web.Hosting.HostingEnvironment.MapPath("~/push/CertificadosPN.p12"));
                _pushBroker.RegisterAppleService(new ApplePushChannelSettings(true, appleCert, "Pa$$w0rd")); //Extension method
            }
        }

        public void SendNotification(MPush mpush )
        {
          
            if (_pushBroker != null)
            {
                try
                {
                   DateTime date = DateTime.Now;
                   string nic = "";
                    if(mpush.tipo == 2){
                        nic = mpush.nic;
                    }

                    _pushBroker.QueueNotification(new AppleNotification()
                                               .ForDeviceToken(mpush.ios_token)
                                               .WithAlert(mpush.mensaje)
                                               .WithBadge(1)
                                               .WithSound("sound")
                                               .WithCustomItem("tipo",mpush.tipo)
                                               .WithCustomItem("nic", nic));

                }
                catch (Exception e)
                {

                    string d = e.ToString();
                }
               
              
             
            }
          //  _pushBroker.StopAllServices();
           
        }

        private void ChannelDestroyed(object sender)
        {
           
        }
        private void ChannelCreated(object sender, PushSharp.Core.IPushChannel pushChannel)
        {
            Debug.WriteLine("cree canal");
            
        }
        private void DeviceSubscriptionChanged(object sender, string oldSubscriptionId, string newSubscriptionId, PushSharp.Core.INotification notification)
        {
            Debug.WriteLine("DeviceSubscriptionChanged");
        }
        private void DeviceSubscriptionExpired(object sender, string expiredSubscriptionId, DateTime expirationDateUtc, PushSharp.Core.INotification notification)
        {
            Debug.WriteLine("DeviceSubscriptionExpired "+ expirationDateUtc.ToString());
        }
        private void NotificationFailed(object sender, PushSharp.Core.INotification notification, Exception error)
        {
            NotificationFailureException ex = (NotificationFailureException)error;

            Debug.WriteLine("fallo " + sender + " -> " + ex.ErrorStatusDescription + " notificacion a enviar " + notification.ToString() + " error code " + ex.ErrorStatusCode);

        }
        private void ServiceException(object sender, Exception error)
        {
            Debug.WriteLine("exception " + error.Message);
        }
           
        private void ChannelException(object sender, PushSharp.Core.IPushChannel pushChannel, Exception error)
        {
            Debug.WriteLine("error del canal " + error.Message);
        }
        private void NotificationSent(object sender, PushSharp.Core.INotification notification)
        {
            Debug.WriteLine("envie la notificiacion " + notification.ToString());
        }
    }
}