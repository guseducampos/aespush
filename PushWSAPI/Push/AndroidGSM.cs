﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;
using PushWSAPI.Models;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
namespace pushDev.Push
{
    public class AndroidGSM
    {

        public string SendNotification(string deviceId, string message)
        {

            string GoogleAppID = "AIzaSyChVMNWyPeL5XEEvSTCTXhjpkBOxV75_8c";
            var SENDER_ID = "672104603814";
            var value = message;
            WebRequest tRequest;
            tRequest = WebRequest.Create("https://android.googleapis.com/gcm/send");
            tRequest.Method = "post";
            tRequest.ContentType = " application/x-www-form-urlencoded;charset=UTF-8";
            tRequest.Headers.Add(string.Format("Authorization: key={0}", GoogleAppID));

            tRequest.Headers.Add(string.Format("Sender: id={0}", SENDER_ID));

            string postData = "collapse_key=score_update&time_to_live=108&delay_while_idle=1&data.titulo=" + value + "&data.time=" +
            System.DateTime.Now.ToString() + "®&registration_id=" + deviceId + "";
            Console.WriteLine(postData);
            Byte[] byteArray = Encoding.UTF8.GetBytes(postData);
            tRequest.ContentLength = byteArray.Length;

            Stream dataStream = tRequest.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            WebResponse tResponse = tRequest.GetResponse();

            dataStream = tResponse.GetResponseStream();

            StreamReader tReader = new StreamReader(dataStream);

            String sResponseFromServer = tReader.ReadToEnd();

            tReader.Close();
            dataStream.Close();
            tResponse.Close();
            return sResponseFromServer;
        }

        public string SendNotificationANDROID(MPush mPush)
        {

            string GoogleAppID = "AIzaSyCiZaOfeEXYjyakQ5gjpRniRv3OpIYBS0c";
            var SENDER_ID = "489511294853";
            var tRequest = (HttpWebRequest)WebRequest.Create("https://android.googleapis.com/gcm/send");
            tRequest.Accept = "application/json";
            tRequest.ContentType = "application/json";
            tRequest.Method = "POST";
            tRequest.Headers.Add(string.Format("Authorization: key={0}", GoogleAppID));
            tRequest.Headers.Add(string.Format("Sender: id={0}", SENDER_ID));

            JObject oComplete = new JObject();
            oComplete["to"] = mPush.android_token;

            JObject oNotification = new JObject();
            oNotification["body"] = mPush.mensaje;
            oNotification["title"] = "AES";//mPush.titulo;
            JObject oData = new JObject();
            if(mPush.tipo == 2 ){
                 oData["nic"] = mPush.nic;
                 oData["tipo"] = 2;
             }
            else
            {

                oData["tipo"] = 1;

            }

            oComplete["notification"] = oNotification;
            oComplete["data"] = oData;
            string parsedContent = oComplete.ToString();
            UTF8Encoding encoding = new UTF8Encoding();
            Byte[] bytes = encoding.GetBytes(parsedContent);

            Stream newStream = tRequest.GetRequestStream();
            newStream.Write(bytes, 0, bytes.Length);
            newStream.Close();

            var response = tRequest.GetResponse();
            var stream = response.GetResponseStream();
            var sr = new StreamReader(stream);
            var content = sr.ReadToEnd();
            string h = content.ToString();
            return h;
        }


    }
}