﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PushWSAPI.Models
{
    public class Notification
    {
        public string sound { get; set; }
        public string badge { get; set; }
        public string body { get; set; }
        public string title { get; set; }
      



    }
}