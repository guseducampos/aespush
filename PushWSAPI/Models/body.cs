﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PushWSAPI.Models
{
    public class body
    {
        public string to { get; set; }

        public bool content_available { get; set; }

       public string priority { get; set; }

        public Notification notification { get; set; }

    }
}