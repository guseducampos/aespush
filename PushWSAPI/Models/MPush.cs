﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace PushWSAPI.Models
{
    //Tipo de dato que espera el servicio web 
     public class MPush
    {

         [Required]
        public string android_token { get; set; }

         [Required]
        public string ios_token { get; set; }

         [Required]
        public string mensaje { get; set; }

        
        public string titulo { get; set; }

         [Required]
        public int tipo { get; set; }

         [Required]
        public string nic { get; set; }
    }
}
