﻿using AESWS;
using pushDev.Push;
using PushNotification;
using PushWSAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;


namespace PushWSAPI.Controllers
{
    public class PushsController : ApiController
    {
     
       
        
        [AcceptVerbs("POST")]
        [HttpPost]
        public RPush Post([FromBody]MPush dataPush)
        {
            RPush push = new RPush();
            try
            {
                if (ModelState.IsValid)
                {
                    AndroidGSM ad = new AndroidGSM();
                    string r = ad.SendNotificationANDROID(dataPush);
                    PushNotificationApple t = new PushNotification.PushNotificationApple();
                    t.SendNotification(dataPush);
                    push.ErrorCode = 0;
                    push.ErrorMessage = "Servicio Ejecutado con exito";
                    push.data = r;
                }
                else {
                    push.ErrorCode = 2;
                    push.ErrorMessage = "Faltan datos";
                    
                }

            }
            catch (Exception e)
            {
                push.ErrorCode = 5;
                push.ErrorMessage = "Ocurrio un error al enviar la notificación";


            }
            return push;
        }


    }
}
