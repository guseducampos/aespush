﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AESWS;
using System.Web.Http.Description;
using pushDev.Push;
using PushNotification;

namespace PushWSAPI.Controllers
{
    public class PushController : ApiController
    {
        // GET: api/Push
       

        // POST: api/Push
        [ResponseType(typeof(Push))]
        public IHttpActionResult SendPush(string android_token, string ios_token,string mensaje)
        {
            Push push = new Push();
            try
            {
                AndroidGSM ad = new AndroidGSM();
                string r = ad.SendNotification(android_token, mensaje);


                PushNotificationApple t = new PushNotification.PushNotificationApple();
                t.SendNotification(ios_token, mensaje);

                push.ErrorCode = 0;
                push.ErrorMessage = "Servicio Ejecutado con exito";
                push.data = r;

            }
            catch (Exception e)
            {
                push.ErrorCode = 5;
                push.ErrorMessage = "Ocurrio un error al enviar la notificación";


            }
            return Ok(push);
        }

      
    }
}
