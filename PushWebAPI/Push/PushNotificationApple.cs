using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PushSharp;
using PushSharp.Apple;
using System.IO;
using System.Diagnostics;

namespace PushNotification
{
    public class PushNotificationApple
    {
        private static PushBroker _pushBroker;

        public PushNotificationApple()
        {
            if (_pushBroker == null)
            {
                //Create our push services broker
                _pushBroker = new PushBroker();

                //Wire up the events for all the services that the broker registers
                _pushBroker.OnNotificationSent += NotificationSent;
                _pushBroker.OnChannelException += ChannelException;
                _pushBroker.OnServiceException += ServiceException;
                _pushBroker.OnNotificationFailed += NotificationFailed;
                _pushBroker.OnDeviceSubscriptionExpired += DeviceSubscriptionExpired;
                _pushBroker.OnDeviceSubscriptionChanged += DeviceSubscriptionChanged;
                _pushBroker.OnChannelCreated += ChannelCreated;
                _pushBroker.OnChannelDestroyed += ChannelDestroyed;
                var appleCert = File.ReadAllBytes(System.Web.Hosting.HostingEnvironment.MapPath("~/push/Certificados.p12"));
                _pushBroker.RegisterAppleService(new ApplePushChannelSettings(false, appleCert, "Pa$$w0rd")); //Extension method
            }
        }

        public void SendNotification(string deviceToken, string message)
        {
          
            if (_pushBroker != null)
            {
                try
                {
                   
                    _pushBroker.QueueNotification(new AppleNotification()
                                               .ForDeviceToken(deviceToken)
                                               .WithAlert(message)
                                               .WithBadge(1)
                                               .WithSound("sound.caf"));

                }
                catch (Exception e)
                {

                    string d = e.ToString();
                }
               
              
             
            }
     
           
        }

        private void ChannelDestroyed(object sender)
        {
           
        }
        private void ChannelCreated(object sender, PushSharp.Core.IPushChannel pushChannel)
        {
            
            
        }
        private void DeviceSubscriptionChanged(object sender, string oldSubscriptionId, string newSubscriptionId, PushSharp.Core.INotification notification)
        {
            
        }
        private void DeviceSubscriptionExpired(object sender, string expiredSubscriptionId, DateTime expirationDateUtc, PushSharp.Core.INotification notification)
        {
           
        }
        private void NotificationFailed(object sender, PushSharp.Core.INotification notification, Exception error)
        {
          

        }
        private void ServiceException(object sender, Exception error)
        {
        }
           
        private void ChannelException(object sender, PushSharp.Core.IPushChannel pushChannel, Exception error)
        {
           
        }
        private void NotificationSent(object sender, PushSharp.Core.INotification notification)
        {
            
        }
    }
}