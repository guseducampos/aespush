﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Configuration;
using System.Data;
using System.Security.Authentication;
using System.IO;
using System.Net;

namespace IOSAPNS
{
    public class IOSAPNS
    {
        String CertificateName = "";
        String CertificatePwd = "";
        String FriendName = "Apple Development IOS Push Services: com.ABC.XYZ";
        String ProductionKeyFriendName = "Production";
        SslStream sslStream;
          

        public IOSAPNS()
        {                 
            FriendName = ConfigurationManager.AppSettings["FriendName"].ToString();
            ProductionKeyFriendName = ConfigurationManager.AppSettings["ProductionKeyFriendName"].ToString();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3;
            
        }

        public bool ConnectToAPNS()
        {
            X509Certificate2Collection certs = new X509Certificate2Collection();

            // Add the Apple cert to our collection
            certs.Add(getServerCert());

            // Apple development server address
            string apsHost;

            if (getServerCert().ToString().Contains(ProductionKeyFriendName))
                apsHost = "gateway.push.apple.com";
            else
                apsHost = "gateway.sandbox.push.apple.com";

            // Create a TCP socket connection to the Apple server on port 2195
            TcpClient tcpClient = new TcpClient(apsHost, 2195);

            // Create a new SSL stream over the connection
            sslStream = new SslStream(tcpClient.GetStream());

            // Authenticate using the Apple cert
            sslStream.AuthenticateAsClient(apsHost, certs, SslProtocols.Tls11, false);

            //PushMessage();

            return true;
        }

        private X509Certificate getServerCert()
        {
            X509Certificate test = new X509Certificate();

            //Open the cert store on local machine        
            X509Store store = new X509Store(StoreName.My, StoreLocation.CurrentUser);

            if (store != null)
            {
                // store exists, so open it and search through the certs for the Apple Cert        
                store.Open(OpenFlags.OpenExistingOnly | OpenFlags.ReadOnly);
                X509Certificate2Collection certs = store.Certificates;

                if (certs.Count > 0)
                {
                    int i;
                    for (i = 0; i < certs.Count; i++)
                    {
                        X509Certificate2 cert = certs[i];

                        if (cert.FriendlyName.Contains(FriendName))
                        {
                            //Cert found, so return it.
                            return certs[i];
                        }
                    }
                }
                return test;
            }
            return test;
        }

        private  byte[] HexToData(string hexString)
        {
            if (hexString == null)
                return null;

            if (hexString.Length % 2 == 1)
                hexString = '0' + hexString; // Up to you whether to pad the first or last byte

            byte[] data = new byte[hexString.Length / 2];

            for (int i = 0; i < data.Length; i++)
                data[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);

            return data;
        }

        public bool PushMessage(string Mess, string DeviceToken, int Badge, string Custom_Field)
        {              
            ConnectToAPNS();     
            List<string> Key_Value_Custom_Field = new List<string>();
            String cToken = DeviceToken;
            String cAlert = Mess;
            int iBadge = Badge;

            // Ready to create the push notification
            byte[] buf = new byte[256];
            MemoryStream ms = new MemoryStream();
            BinaryWriter bw = new BinaryWriter(ms);
            bw.Write(new byte[] { 0, 0, 32 });

            byte[] deviceToken = HexToData(cToken);
            bw.Write(deviceToken);

            bw.Write((byte)0);

            // Create the APNS payload - new.caf is an audio file saved in the application bundle on the device
            string msg = "";       
           msg = "{\"aps\":{\"alert\":\"" + cAlert + "\",\"badge\":\"" + iBadge.ToString() + "\",\"sound\":\"noti.aiff\"}";
        
            String PayloadMess = "";
            if (string.IsNullOrWhiteSpace(Custom_Field) == false)
            {
                List<string> list_Custom_Field = Custom_Field.Split(';').ToList();

                if (list_Custom_Field.Count > 0)
                {
                    for (int indx = 0; indx < list_Custom_Field.Count; indx++)
                    {
                        Key_Value_Custom_Field = list_Custom_Field[indx].Split('=').ToList();
                        if (Key_Value_Custom_Field.Count > 1)
                        {
                            if (PayloadMess != "") PayloadMess += ", ";                        
                            PayloadMess += "\"" + Key_Value_Custom_Field[0].ToString() + "\":\"" + Key_Value_Custom_Field[1].ToString() + "\"";
                        }
                    }
                }
            }

            if (PayloadMess != "")
            {
                msg += ", " + PayloadMess;
            }
            msg += "}";

            // Write the data out to the stream
            bw.Write((byte)msg.Length);
            bw.Write(msg.ToCharArray());
            bw.Flush();

            if (sslStream != null)
            {
                sslStream.Write(ms.ToArray());
                return true;
            }

            return false;
        }

        private void pushMessageTest(string id, string msg)
        {
            int port = 2195;
            String deviceID = "14102fae4e653664b1bd1faff04068c8a53558531b5e1014932931f3f956be4c";
            String hostname = "gateway.sandbox.push.apple.com";     // TEST
            //String hostname = "gateway.push.apple.com";           // REAL

            //        @"cert.p12";
            String certificatePath = HttpContext.Current.Server.MapPath("~/Push/Certificados.p12");
            //X509Certificate2 clientCertificate = new X509Certificate2(certificatePath, "");

            X509Certificate2 clientCertificate = new X509Certificate2(System.IO.File.ReadAllBytes(certificatePath), "Pa$$w0rd", X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.PersistKeySet | X509KeyStorageFlags.Exportable);

            X509Certificate2Collection certificatesCollection = new X509Certificate2Collection(clientCertificate);
            TcpClient client = new TcpClient(hostname, port);

            // _apnsStream = new SslStream(_apnsClient.GetStream(), false, validateServerCertificate, SelectLocalCertificate);
            //SslStream sslStream = new SslStream(client.GetStream(), false, new RemoteCertificateValidationCallback(ValidateServerCertificate),null);

            SslStream sslStream = new SslStream(client.GetStream(), false, validateServerCertificate, SelectLocalCertificate);
            try
            {
                sslStream.AuthenticateAsClient(hostname, certificatesCollection, SslProtocols.Default, false);
            }
            catch (Exception e)
            {
                throw (e);
                client.Close();
                return;
            }

            MemoryStream memoryStream = new MemoryStream();
            BinaryWriter writer = new BinaryWriter(memoryStream);
            writer.Write((byte)0);  //The command
            writer.Write((byte)0);  //The first byte of the deviceId length (big-endian first byte)
            writer.Write((byte)32); //The deviceId length (big-endian second byte)

            writer.Write(HexStringToByteArray(deviceID.ToUpper()));
            String payload = "{\"aps\":{\"alert\":\"hello\",\"badge\":0,\"sound\":\"default\"}}";
            writer.Write((byte)0);
            writer.Write((byte)payload.Length);
            byte[] b1 = System.Text.Encoding.UTF8.GetBytes(payload);
            writer.Write(b1);
            writer.Flush();
            byte[] array = memoryStream.ToArray();
            sslStream.Write(array);
            sslStream.Flush();
            client.Close();
        }

        private X509Certificate SelectLocalCertificate(object sender, string targetHost, X509CertificateCollection localCertificates, X509Certificate remoteCertificate, string[] acceptableIssuers)
        {
            throw new NotImplementedException();
        }

        private bool validateServerCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            throw new NotImplementedException();
        }

        private string HexStringToByteArray(string p)
        {
            throw new NotImplementedException();
        }
    }
}